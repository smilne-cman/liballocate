#include <stdio.h>
#include <stdlib.h>
#include <libtest/libtest.h>
#include <libcollection/list.h>
#include "liballocate.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
static void before_each();

static void test_transaction();
static void test_current();
static void test_size();
static void test_find();

static void mock_destructor(void *item);

/* Global Variables ***********************************************************/
static int destructor_called = FALSE;
static void *destructor_called_with = NULL;

int main(int argc, char *argv[]) {
  test_init("liballocate", argc, argv);
  test_before_each(before_each);

  test("Transaction", test_transaction);
  test("Current Transaction", test_current);
  test("Transaction Size", test_size);
  test("Find Transaction", test_find);

  return test_complete();
}

static void before_each() {
  destructor_called = FALSE;
  destructor_called_with = NULL;
}

static void test_transaction() {
  Transaction *transaction1 = transaction_push();
  Transaction *transaction2 = transaction_push();

  test_assert(transaction2->parent == transaction1, "should set the parent transaction");
  test_assert(current_transaction() == transaction2, "should return latest transaction as current");

  char *message1 = allocate(sizeof(char) * 10, mock_destructor);
  char *message2 = allocate(sizeof(char) * 10, mock_destructor);
  test_assert_int(list_size(transaction2->items), 2, "should add allocations to the new transaction");

  promote(message2);
  test_assert_int(list_size(transaction1->items), 1, "should add promoted item to parent transaction");
  test_assert_int(list_size(transaction2->items), 1, "should remove promoted item from current transaction");
  test_assert_int(stack_size(), 20, "should not change overall stack size");

  MemoryAllocation *allocation = list_get(transaction1->items, 0);
  test_assert(allocation->item == message2, "should promote correct item");

  transaction_pop();
  test_assert(current_transaction() == transaction1, "should revert to parent transaction when popped");
  test_assert(destructor_called == TRUE, "should deallocate items in poped transaction");
  test_assert(destructor_called_with == message1, "should deallocate non-promoted item");
  test_assert_int(list_size(transaction1->items), 1, "should not destroy promoted items");

  transaction_pop();
}

static void test_current() {
  Transaction *transaction1 = transaction_push();
  Transaction *transaction2 = transaction_push();

  test_assert(current_transaction() == transaction2, "should return the latest transaction");

  transaction_pop();
  transaction_pop();
  transaction_pop();

  test_assert(current_transaction() != NULL, "should return a new transaction if no active transaction");
}

static void test_size() {
  Transaction *transaction = transaction_new(NULL);

  char *message1 = allocate_in(transaction, sizeof(char) * 10, NULL);
  char *message2 = allocate_in(transaction, sizeof(char) * 5, NULL);

  test_assert_int(transaction_size(transaction), 15, "should return total size of all items");

  transaction_destroy(transaction);
}

static void test_find() {
  Transaction *transaction1 = transaction_push();

  char *message1 = allocate(sizeof(char) * 10, NULL);
  char *message2 = malloc(sizeof(char) * 10);

  Transaction *transaction2 = transaction_push();

  test_assert(transaction_find(message1) == transaction1, "Should find the transaction that the item is part of");
  test_assert(transaction_find(message2) == NULL, "Should return NULL if transaction cannot be found");
}

static void mock_destructor(void *item) {
  destructor_called = TRUE;
  destructor_called_with = item;
}