#include <stdio.h>
#include <stdlib.h>
#include <libcollection/list.h>
#include <libcollection/iterator.h>
#include "liballocate.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
static MemoryAllocation *splice_allocation(Transaction *transaction, void *item);
static int allocation_for_item(MemoryAllocation *value, void *item);
static void free_allocation(MemoryAllocation *allocation);

/* Global Variables ***********************************************************/
static Transaction *root_transaction = NULL;

/* Functions ******************************************************************/
void *allocate(size_t size, Destructor destructor) {
  return allocate_in(current_transaction(), size, destructor);
}

void *allocate_in(Transaction *transaction, size_t size, Destructor destructor) {
  MemoryAllocation *allocation = (MemoryAllocation *)malloc(sizeof(MemoryAllocation));

  allocation->destructor = destructor;
  allocation->size = size;
  allocation->item = malloc(size);

  list_add(transaction->items, allocation);

  return allocation->item;
}

void deallocate(void *item) {
  Transaction *owner = transaction_find(item);
  if (owner == NULL) {
    return;
  }

  MemoryAllocation *allocation = splice_allocation(owner, item);
  if (allocation != NULL) {
    free_allocation(allocation);
  }
}

void promote(void *item) {
  Transaction *current = current_transaction();
  Transaction *parent = current->parent;

  if (parent == NULL) {
    return;
  }

  MemoryAllocation *allocation = splice_allocation(current, item);

  if (allocation != NULL) {
    list_add(parent->items, allocation);
  }
}

Transaction *current_transaction() {
  if (root_transaction == NULL) {
    transaction_push();
  }

  return root_transaction;
}

Transaction *transaction_push() {
  Transaction *transaction = transaction_new(root_transaction);

  root_transaction = transaction;

  return transaction;
}

void transaction_pop() {
  Transaction *transaction = current_transaction();
  root_transaction = transaction->parent;
  
  transaction_destroy(transaction);
}

size_t stack_size() {
  size_t total = 0;

  Transaction *transaction = current_transaction();
  do{
    total += transaction_size(transaction);

    transaction = transaction->parent;
  } while(transaction != NULL);

  return total;
}

Transaction *transaction_new(Transaction *parent) {
  Transaction *transaction = (Transaction *)malloc(sizeof(Transaction));

  transaction->parent = parent;
  transaction->items = list_new(ListAny);

  return transaction;
}

Transaction *transaction_find(void *item) {
  Transaction *current = current_transaction();
  int index = -1;

  while(index < 0) {
    index = list_index_where(current->items, item, allocation_for_item);

    if (index < 0 && current->parent != NULL) {
      current = current->parent;
    } else {
      break;
    }
  }

  if (index < 0) {
    return NULL;
  }

  return current;
}

void transaction_destroy(Transaction *transaction) {
  Iterator *iterator = list_iterator(transaction->items);
  
  while(has_next(iterator)) {
    MemoryAllocation *allocation = (MemoryAllocation *)next(iterator);

    free_allocation(allocation);
  }

  iterator_destroy(iterator);
  list_destroy(transaction->items);
  free(transaction);
}

size_t transaction_size(Transaction *transaction) {
  Iterator *iterator = list_iterator(transaction->items);
  size_t total = 0;

  while(has_next(iterator)) {
    MemoryAllocation *allocation = next(iterator);

    total += allocation->size;
  }

  return total;
}

static MemoryAllocation *splice_allocation(Transaction *transaction, void *item) {
  int index = list_index_where(transaction->items, item, allocation_for_item);

  if (index < 0) {
    return NULL;
  }

  MemoryAllocation *allocation = list_get(transaction->items, index);
  list_remove(transaction->items, index);

  return allocation;
}

static void free_allocation(MemoryAllocation *allocation) {
  if (allocation->destructor != NULL) {
    allocation->destructor(allocation->item);
  } else {
    free(allocation->item);
  }

  free(allocation);
}

static int allocation_for_item(MemoryAllocation *value, void *item) {
  return value->item == item ? EQUAL : LESS;
}
