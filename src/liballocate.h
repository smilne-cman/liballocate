#ifndef __liballocate__
#define __liballocate__

/* Includes *******************************************************************/
#include <libcollection/list.h>

/* Types **********************************************************************/
typedef void (*Destructor)(void *item);

typedef struct Transaction {
  struct Transaction *parent;
  List *items;
} Transaction;

typedef struct MemoryAllocation {
  Destructor destructor;
  size_t size;
  void *item;
} MemoryAllocation;

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/
void *allocate(size_t size, Destructor destructor);
void *allocate_in(Transaction *transaction, size_t size, Destructor destructor);

void deallocate(void *item);
void promote(void *item);

Transaction *current_transaction();
Transaction *transaction_push();
void transaction_pop();
size_t stack_size();

Transaction *transaction_new(Transaction *parent);
Transaction *transaction_find(void *item);
void transaction_destroy(Transaction *transaction);
size_t transaction_size(Transaction *transaction);

#endif
